#! /usr/bin/env python

import re
import sys
import yaml

def escape(text):
    return re.sub('([][\\`*_{}<>()#+-.!|])', '\\\\\\1', text)

leaderboard = yaml.safe_load(sys.stdin)

for ((pos, item), next_item) in reversed(list(zip(reversed(list(enumerate(leaderboard))), list(reversed([None] + leaderboard))[1:]))):
    name = item['name']
    prestige = item['prestige']

    print(f'  * **#{pos + 1}  {escape(name)}:** {prestige}')
    if next_item is not None:
        print()
        print(f"    *remaining {next_item['prestige'] - prestige} to beat “{escape(next_item['name'])}”*")
        print()
    print()
