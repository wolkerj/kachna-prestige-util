Kachna prestige util
====================

[Zobrazit výstup](https://jwo.cz/fit/prestige-util/all.txt)

Skript (Python + Makefile), který stahuje aktuální stav prestiže
v Kachně a vytváří texťák s informacemi o tom, kolik která osoba
(z prvních deseti) musí získat bodů, aby postoupila na vyšší pozici.

Jde o velmi narychlo sepsaný skript.
