all: index.html README.html

%.html: %.md
	markdown $< > $@
	echo '<p style="font-size: 1.5em; color:#3498DB; text-shadow: 2px 2px 3px #fff">Idea from awesome fox <a href="https://pizzachips.xyz">@PizzaChips</a></p>' >> $@

index.md: remaining-to.all.md
	cat $+ > $@
	echo >> $@
	echo -n "*Generated at: $(shell date)*" >> $@

remaining-to.all.md: remaining-to.today.md remaining-to.semester.md
	cat $+ >$@
	echo >>$@

remaining-to.%.md: %.json
	echo "Number of prestige points to gain ($*):" >$@
	echo "===============================================" >>$@
	python3 remaining_pos.py < $< >> $@
	echo >> $@

today.json semester.json: %.json:
	curl 'https://www.su.fit.vutbr.cz/kachna/api/club/leaderboard/'$* > $@
	mkdir -p archive
	cp $@ archive/$@.$(shell date -Iminutes)
